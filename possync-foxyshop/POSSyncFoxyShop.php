<?php
    /*
    Plugin Name: POSSync with Foxyshop
    Plugin URI:  https://bitbucket.org/ericatnahnay/pos-sync-foxyshop-wordpress-plugin
    Description: This plugin to allows POSSync to update inventory levels used by FoxyShop
    Author: Nahnay
    Version: 0.1
    Author URI:   https://www.pos-sync.com
    */

    // POSSync with Foxyshop
    /* Copyright 2013 Nahnay (email: dev@nahnay.com)   - MIT License

    Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
    documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
    Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
    WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
    OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
    OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    */


    // options code based on http://wordpress.org/extend/plugins/plugin-options-starter-kit/

    // Plugin Options Starter Kit copyright included here to satisfy license requirements
    /* Copyright 2009 David Gwyer (email : d.v.gwyer@presscoders.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// ------------------------------------------------------------------------
// MAIN ACTIONS
// ------------------------------------------------------------------------
// MAIN PLUGIN ACTIONS, BESIDES ADMIN FUNCTIONS
// ------------------------------------------------------------------------

// Handles POSSync requests
add_action( 'init', 'possyncfoxyshop_handleAPIRequest' );
add_action( 'send_headers' , 'possyncfoxyshop_sendheaders' );


// ------------------------------------------------------------------------
// REQUIRE MINIMUM VERSION OF WORDPRESS:
// ------------------------------------------------------------------------
// THIS IS USEFUL IF YOU REQUIRE A MINIMUM VERSION OF WORDPRESS TO RUN YOUR
// PLUGIN. IN THIS PLUGIN THE WP_EDITOR() FUNCTION REQUIRES WORDPRESS 3.3
// OR ABOVE. ANYTHING LESS SHOWS A WARNING AND THE PLUGIN IS DEACTIVATED.
// ------------------------------------------------------------------------

function requires_wordpress_version() {
    global $wp_version;
    $plugin = plugin_basename( __FILE__ );
    $plugin_data = get_plugin_data( __FILE__, false );

    if ( version_compare($wp_version, "3.3", "<" ) ) {
        if( is_plugin_active($plugin) ) {
            deactivate_plugins( $plugin );
            wp_die( "'".$plugin_data['Name']."' requires WordPress 3.3 or higher, and has been deactivated! Please upgrade WordPress and try again.<br /><br />Back to <a href='".admin_url()."'>WordPress admin</a>." );
        }
    }
}
function requires_foxyshop_plugin(){
    $plugin = plugin_basename( __FILE__ );
    $plugin_data = get_plugin_data( __FILE__, false );
    $foxyshop_plugin_path = "foxyshop/foxyshop.php";
    if (!is_plugin_active($foxyshop_plugin_path )){
        deactivate_plugins($plugin);
        wp_die( "'".$plugin_data['Name']."' requires the FoxyShop plugin, and has been deactivated! Please install and activate the FoxyShop plugin.<br /><br />Back to <a href='".admin_url()."'>WordPress admin</a>." );
    }
}

add_action( 'admin_init', 'requires_wordpress_version' );
add_action( 'admin_init', 'requires_foxyshop_plugin' );


// ------------------------------------------------------------------------
// REGISTER HOOKS & CALLBACK FUNCTIONS:
// ------------------------------------------------------------------------
// HOOKS TO SETUP DEFAULT PLUGIN OPTIONS, HANDLE CLEAN-UP OF OPTIONS WHEN
// PLUGIN IS DEACTIVATED AND DELETED, INITIALISE PLUGIN, ADD OPTIONS PAGE.
// ------------------------------------------------------------------------

// Set-up Action and Filter Hooks
register_activation_hook(__FILE__, 'possyncfoxyshop_add_defaults');
register_uninstall_hook(__FILE__, 'possyncfoxyshop_delete_plugin_options');
add_action('admin_init', 'possyncfoxyshop_init' );
add_action('admin_menu', 'possyncfoxyshop_add_options_page');
add_filter( 'plugin_action_links', 'possyncfoxyshop_plugin_action_links', 10, 2 );

// --------------------------------------------------------------------------------------
// CALLBACK FUNCTION FOR: register_uninstall_hook(__FILE__, 'possyncfoxyshop_delete_plugin_options')
// --------------------------------------------------------------------------------------
// THIS FUNCTION RUNS WHEN THE USER DEACTIVATES AND DELETES THE PLUGIN. IT SIMPLY DELETES
// THE PLUGIN OPTIONS DB ENTRY (WHICH IS AN ARRAY STORING ALL THE PLUGIN OPTIONS).
// --------------------------------------------------------------------------------------

// Delete options table entries ONLY when plugin deactivated AND deleted
function possyncfoxyshop_delete_plugin_options() {
    delete_option('possyncfoxyshop_options');
}

// ------------------------------------------------------------------------------
// CALLBACK FUNCTION FOR: register_activation_hook(__FILE__, 'possyncfoxyshop_add_defaults')
// ------------------------------------------------------------------------------
// THIS FUNCTION RUNS WHEN THE PLUGIN IS ACTIVATED. IF THERE ARE NO THEME OPTIONS
// CURRENTLY SET, OR THE USER HAS SELECTED THE CHECKBOX TO RESET OPTIONS TO THEIR
// DEFAULTS THEN THE OPTIONS ARE SET/RESET.
//
// OTHERWISE, THE PLUGIN OPTIONS REMAIN UNCHANGED.
// ------------------------------------------------------------------------------

// Define default option settings
function possyncfoxyshop_add_defaults() {
    $tmp = get_option('possyncfoxyshop_options');
    $apikey = get_option('possyncfoxyshop_apikey');
    if (!$apikey){
        possyncfoxyshop_generateApiKey();
    }
}


// ------------------------------------------------------------------------------
// CALLBACK FUNCTION FOR: add_action('admin_init', 'possyncfoxyshop_init' )
// ------------------------------------------------------------------------------
// THIS FUNCTION RUNS WHEN THE 'admin_init' HOOK FIRES, AND REGISTERS YOUR PLUGIN
// SETTING WITH THE WORDPRESS SETTINGS API. YOU WON'T BE ABLE TO USE THE SETTINGS
// API UNTIL YOU DO.
// ------------------------------------------------------------------------------

// Init plugin options to white list our options
function possyncfoxyshop_init(){
    register_setting( 'possyncfoxyshop_plugin_options', 'possyncfoxyshop_options', 'possyncfoxyshop_validate_options' );
}

// ------------------------------------------------------------------------------
// CALLBACK FUNCTION FOR: add_action('admin_menu', 'possyncfoxyshop_add_options_page');
// ------------------------------------------------------------------------------
// THIS FUNCTION RUNS WHEN THE 'admin_menu' HOOK FIRES, AND ADDS A NEW OPTIONS
// PAGE FOR YOUR PLUGIN TO THE SETTINGS MENU.
// ------------------------------------------------------------------------------

// Add menu page
function possyncfoxyshop_add_options_page() {
    add_options_page('POS-Sync + FoxyShop Options Options Page', 'POS-Sync + FoxyShop Options', 'manage_options', __FILE__, 'possyncfoxyshop_render_form');
}

// ------------------------------------------------------------------------------
// CALLBACK FUNCTION SPECIFIED IN: add_options_page()
// ------------------------------------------------------------------------------
// THIS FUNCTION IS SPECIFIED IN add_options_page() AS THE CALLBACK FUNCTION THAT
// ACTUALLY RENDER THE PLUGIN OPTIONS FORM AS A SUB-MENU UNDER THE EXISTING
// SETTINGS ADMIN MENU.
// ------------------------------------------------------------------------------

// Render the Plugin options form
function possyncfoxyshop_render_form() {
    ?>
<div class="wrap">

    <!-- Display Plugin Icon, Header, and Description -->
    <div class="icon32" id="icon-options-general"><br></div>
    <h2>POS-Sync + FoxyShop Options</h2>

    <!-- Beginning of the Plugin Options Form -->
    <form method="post" action="options.php">
        <?php settings_fields('possyncfoxyshop_plugin_options'); ?>
        <?php $apikey = get_option('possyncfoxyshop_apikey'); ?>

        <p></p><strong>API key: </strong><span><?php echo $apikey; ?></span></p>

        <p class="submit">
            <input type="submit" class="button-primary" value="<?php _e('Generate new API key') ?>" />
        </p>
    </form>

</div>
<?php
}

function possyncfoxyshop_validate_options($input) {
    possyncfoxyshop_generateApiKey();
    return $input;
}

// Display a Settings link on the main Plugins page
function possyncfoxyshop_plugin_action_links( $links, $file ) {

    if ( $file == plugin_basename( __FILE__ ) ) {
        $possyncfoxyshop_links = '<a href="'.get_admin_url().'options-general.php?page=POSSyncFoxyShop/POSSyncFoxyShop.php">'.__('Settings').'</a>';
        // make the 'Settings' link appear first
        array_unshift( $links, $possyncfoxyshop_links );
    }

    return $links;
}

// ------------------------------------------------------------------------------
// CALLBACK FUNCTION FOR: add_action('init', 'possyncfoxyshop_handleAPIRequest' )
// ------------------------------------------------------------------------------
// THIS FUNCTION HANDLES POSSYNC API REQUESTS
// ------------------------------------------------------------------------------

function possyncfoxyshop_handleAPIRequest(){
    if (isPOSSync()){
        if (!isset($_GET['fn'])){
            return false;
        }
        $fn = $_GET['fn'];
        $json = array();

        switch ($fn){
            case 'products':
                $json = possyncfoxyshop_get_products();
                break;
            case 'product':
                if (!isset($_GET['id'])){
                    return false;
                }
                $id = $_GET['id'];
                array_push($json, possyncfoxyshop_get_product($id));
                break;

            case 'update_product':
                $id = $_GET['id'];
                $quantity =  $_GET['quantity'];
                $json['status'] = possyncfoxyshop_update_product($id,$quantity);
                break;
            case 'test':
                $json['key'] = "testkey";
                $json['value'] = "testvalue";
                break;
        }
        possyncfoxyshop_jsondumps($json);
        die;
    }
}

function possyncfoxyshop_sendheaders(){
    if (isPOSSync()){
        header('Content-type: application/json;');
    }
}


// ------------------------------------------------------------------------------
// Helper functions:
// ------------------------------------------------------------------------------
// Helper functions.
// ------------------------------------------------------------------------------
// Query string
// echo "<pre>"; print_r($wp_query->query_vars); echo "</pre>";

// Function to generate and save API key
function possyncfoxyshop_generateApiKey(){
    $apikey = hash('sha256',get_current_user() . "\0" . microtime());
    update_option('possyncfoxyshop_apikey',$apikey);
}

// Checks to see if the page is called from POS-Sync
function isPOSSync(){
    if (isset($_GET['possyncapi'])){
        $apikey = $_GET['possyncapi'];
        return possyncfoxyshop_validateAPIKey($apikey);
    }
    return false;
}

// Function to validate api key
function possyncfoxyshop_validateAPIKey($apikey){
    if (!$apikey==get_option('possyncfoxyshop_apikey')){
        return false;
    }
    return true;
}

function possyncfoxyshop_jsondumps($json){
    echo json_encode($json);
}

// Creates the structure of a product for the return data
function possyncfoxyshop_build_product($post){
    $product = array();

    $product['id'] = $post->ID;
    $product['name'] = $post->post_name;

    $inventory = get_post_meta($post->ID,'_inventory_levels',1);

    foreach(array_keys($inventory) as $code){
        //echo 'inventory level for ' . $code . ' is ' . implode(';',array_keys($inventory[$code]));
        $product['sku'] = $code;
        $product['quantity'] = $inventory[$code]['count'];
        break;

    };
    return $product;
}

// Gets all products
function possyncfoxyshop_get_products(){
    $args = array('post_type' => 'foxyshop_product', 'post_status' => 'publish', 'posts_per_page' => get_query_var('per_page'), 'paged' => get_query_var('page'));
    $the_query = new WP_Query($args);
    $products = array();
    while ($the_query->have_posts()){
        $the_query->the_post();

        // build out the return value
        // using get_post_meta to get the inventory levels

        array_push($products, possyncfoxyshop_build_product($the_query->post));
    }
    return $products;
}

// Gets a product by id
function possyncfoxyshop_get_product($id){
    $post = get_post($id);
    if ($post->post_type == "foxyshop_product"){
        return possyncfoxyshop_build_product($post);
    }
    return null;
}

// Updates a product
function possyncfoxyshop_update_product($id, $quantity){
    $post = possyncfoxyshop_get_product($id);
    if ($post){
        $inventory = array();
        $inventory[$post['sku']]['count'] = $quantity;
        //Update
        update_post_meta($id, '_inventory_levels', $inventory);
        return true;
    }
    return false;
}

