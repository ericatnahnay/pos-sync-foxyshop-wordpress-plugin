=== Plugin Name ===
Contributors: nahnay
Tags: foxyshop, inventory, synchronization
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 0.1
License: GPLv2 or later
License URI: http://opensource.org/licenses/MIT

POS-Sync and FoxyShop plugin for inventory management.

== Description ==
This plugin builds upon the awesome Wordpress plugin called FoxyShop (http://wordpress.org/extend/plugins/foxyshop/) which morphs
your Wordpress site into an online store with full integration to FoxyCart (http://www.foxycart.com) provide a seemless
checkout experience.

This plugin synchronizes inventory from POS-Sync connected POS (Point of Sale) sysetms to your FoxyShop site.  For the
full story on POS-Sync check out https://www.pos-sync.com and for the list of supported POS systems check out
https://www.pos-sync.com/partners.

== Installation ==

This section describes how to install the plugin and get it working.

1. Download <a href="https://bitbucket.org/ericatnahnay/pos-sync-foxyshop-wordpress-plugin/downloads">latest version</a>
 of the plugin.
1. Open your Wordpress Admin panel, click on Plugins and click on "Add New".
1. Click on "Upload".
1. Click on "Choose File" and choose the file POS-Sync+FoxyShop plugin which was downloaded previously. Click on
 "Install Now" when done.
1. Once the POS-Sync+FoxyShop plugin is installed, click on "Activate Plugin" to activate the plugin.
1. On the plugins screen, scroll down to the POS-Sync+FoxyShop plugin and click on "Settings.
1. The API Key will be displayed.
1. Copy the API Key and use this key during <a href="https://www.pos-sync.com/profile/appsettings">account setup on
 POS-Sync.

== Frequently Asked Questions ==

= Do I have to have FoxyShop installed? =

Yes.  This plugin is based on the FoxyShop plugin (http://wordpress.org/extend/plugins/foxyshop/)



== Changelog ==

= 0.1 =
* Initial
