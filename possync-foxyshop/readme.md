POS-Sync + FoxyShop Wordpress Plugin
************************************

This plugin builds upon the awesome Wordpress plugin called FoxyShop (http://wordpress.org/extend/plugins/foxyshop/) which morphs
your Wordpress site into an online store with full integration to FoxyCart (http://www.foxycart.com) provide a seemless
checkout experience.

This plugin synchronizes inventory from POS-Sync connected POS (Point of Sale) sysetms to your FoxyShop site.  For the
full story on POS-Sync check out https://www.pos-sync.com and for the list of supported POS systems check out
https://www.pos-sync.com/partners.